import os

from setuptools import setup, find_namespace_packages


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(filename):
    return open(os.path.join(os.path.dirname(__file__), filename)).read()


setup(
    name='kypo-openstack-lib',
    author='KYPO Team',
    description='KYPO Openstack Driver',
    long_description=read('README.md'),
    packages=find_namespace_packages(include=['kypo.*'], exclude=['tests']),
    package_data={'kypo.openstack_driver': ['templates/*']},
    install_requires=[
        'kypo-python-commons==0.1.*',
        'keystoneauth1',
        'jinja2',
        'netaddr',
        'python-glanceclient>=2.17',
        'python-keystoneclient>=3.22',
        'python-neutronclient>=6.14',
        'python-novaclient>=16.0',
        'structlog',
        'Jinja2',
        'pycryptodome>=3.9',
        'deepdiff',
        'yamlize>=0.6',
    ],
    python_requires='>=3',
    zip_safe=False
)
